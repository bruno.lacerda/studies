#include "tools.h"

#define ONE_BILLION 10000000000
typedef unsigned long long int big_n;

void
while_sum_short(big_n n) {
  big_n sum = 0;
  size_t i = 0;

  while (i < n) { sum += i++; }
}

void
while_sum_long(big_n n) {
  big_n sum = 0;
  size_t i = 0;

  while (i < n) {
    sum += i;
    i++;
  }
}

void
for_sum_short(big_n n) {
  big_n sum = 0;

  for (size_t i = 0; i < n;) {
    sum += i++;
  }
}

void
for_sum_long(big_n n) {
  big_n sum = 0;

  for (size_t i = 0; i < n; i++) {
    sum += i;
  }
}

/*
 * OS: Windows 11
 *
 * CPU: AMD Ryzen 5 3600 - 3.95 GHz (Underclock)
 *
 * Average Results [Runs = 4]
 *
 * while - short -> 6.421
 *   for - short -> 6.327
 * while - long  -> 9.876
 *   for - long  -> 9.903
 *
 * ===============================================
 *
 * CPU: AMD Ryzen 9 5900X - (3.7 - 5.0 GHz Auto OC)
 * 
 * Average Results [Runs = 2]
 * 
 * while - short -> 4.694
 *   for - short -> 4.769
 * while - long  -> 6.733
 *   for - long  -> 6.703
 */

int
main() {
  big_n n = ONE_BILLION;
  clock_t t;

  printf("N = %llu\n", n);

  t = clock();
  while_sum_short(n);
  t = clock() - t;
  printf("\n[while - short] Time taken: %.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);

  t = clock();
  for_sum_short(n);
  t = clock() - t;
  printf("\n[  for - short] Time taken: %.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);

  t = clock();
  while_sum_long(n);
  t = clock() - t;
  printf("\n[while - long ] Time taken: %.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);

  t = clock();
  for_sum_long(n);
  t = clock() - t;
  printf("\n[  for - long ] Time taken: %.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);

  return 0;
}
