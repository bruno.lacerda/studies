fw: time-for-while.c tools.h
	gcc time-for-while.c -o fw.out
	./fw.out

fwt: for-while-thread.c tools.h
	gcc for-while-thread.c -o fwt.out -pthread
	./fwt.out

invsqrt: inv-sqrt.c tools.h
	gcc inv-sqrt.c -o invsqrt.out
	./invsqrt.out

lnxhpw: linux-hidepass.c lnxhpw.h
	gcc linux-hidepass.c -o lnxhpw.out
	./lnxhpw.out
