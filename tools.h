#ifndef tools_H
#define tools_H

//-------------------------------------------------------------------------------------------------------------//

  // Libraries

  #include <time.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <iso646.h>

//-------------------------------------------------------------------------------------------------------------//

  // Custom Types Definition

  typedef enum bool { false, true } bool;
  typedef unsigned int uint;
  typedef char* string;
  typedef void* var;

  #define HUN_THOUSAND 100000
  #define ONE_MILLION  1000000
  #define TEN_MILLION  10000000

//-------------------------------------------------------------------------------------------------------------//
  
  // MACROS

  //
  // Unimplemented function, aborts execution
  //
  #define UNIMPLEMENTED \
    do { \
      fprintf(stderr, "\n%s:%d: %s is not implemented yet...\n\n", \
              __FILE__, __LINE__, __func__); \
      abort(); \
    } while (0)

  //
  // Prints the name of the function being called
  //
  #define CALLING printf("\n\n||===== Calling: %s =====||\n", __func__)

  //
  // Default text for FILE open error
  //
  #define FO_ERROR "Couldn't open "

  //
  // Returns max of two elements
  //
  #define max(a, b)  (((a) >= (b)) ? (a) : (b))

//-------------------------------------------------------------------------------------------------------------//

  // Methods and Functions

  //
  // Clears stdin by reading its content until a \n is found
  //
  void clearBuffer() { while(getchar() != '\n') {} }

  //
  // Waits for enter key to be pressed
  //
  void pause() { fputs("\nPress <enter> to continue... ", stdout); getchar(); }

  //
  // Sets the cursor line's position to the
  // northwestern-most part of the screen 
  // Similar to 'cls' or Ctrl + L
  //
  void clearScreen() { fputs("\e[1;1H\e[2J", stdout); }

  //
  // Returns a pointer to an array of chars with the passed size in bytes
  //
  string String(const size_t size) { return (string) calloc(size, sizeof(char)); }

  //
  // Swaps the content of any two pointers of the same type or with the same content size
  //
  void
  swap_void(var n1, var n2, size_t size) {
    var temp = calloc(1, size);

    memcpy(temp, n1, size);
    memcpy(n1,   n2, size);
    memcpy(n2, temp, size);

    free(temp);
  }

  //
  // Swaps the content of two int addresses
  //
  void
  swap(int* n1, int* n2) {
    int temp;
    temp = *n1;
    *n1  = *n2;
    *n2  = temp;
  }

  //
  // Swaps the content of two int addresses
  // The assembler will insert these instructions in the function
  // that called this one. There's no actual function call
  //
  static __attribute__((__always_inline__)) inline void
  swap_inline(int* n1, int* n2) {
    int temp;
    temp = *n1;
    *n1  = *n2;
    *n2  = temp;
  }

  //
  // Simple HH/MM/SS hour validation
  //
  bool
  isValidTime(const int hour, const int min, const int sec) {
    if (hour < 0 or hour > 24)
      return false;

    if (min < 0 or min > 59)
      return false;

    if (sec < 0 or sec > 59)
      return false;

    return true;
  }

  //
  // Reads the input number
  // as a sequence of chars.
  // Later parsed to a single int
  //
  int
  Int() {
    int num  = 0;
    int sign = 1;
    int c;

    while ((c = getchar()) != '\n') {
      if (c == '-')
        sign = -1;

      else if (c >= '0' and c <= '9')
        num = num * 10 + (c - '0');
    }

    return num * sign;
  }

  //
  // Reads a number as a string and parses it to double
  //
  double
  Double() {
    char input[100];
    fgets(input, 100, stdin);

    string endPtr;
    double result = strtod(input, &endPtr);

    if (input == endPtr) {
      puts("No number read... set to default value (0.0)");
      return 0.0;
    }

    return result;
  }

  //
  // Prints a formatted CPF
  //
  void
  PrintCPF(char const *cpf) {
    short i = 0;

    while (cpf[i] != '\0' and i < 11) {
      putchar(cpf[i]);

      switch(i++) {
        case 2:
        case 5: putchar('.'); break;
        case 8: putchar('/'); break;
      }
    }
  }

  //
  // Prints a formatted car's id plate
  //
  void
  PrintPlaca(char const *placa) {
    short i = 0;

    while (placa[i] != '\0' and i < 7) {
      if (i == 3) putchar('-');
      putchar(placa[i++]);
    }
  }

  //
  // Prints an error message regarding a FILE operation
  // Concatenates both passed strings into the message perror() prints
  //
  void
  fprint_err(char const *predicate, char const *filename) {
    char e[50] = {0};
    char* error = e;
    strcat(error, predicate);
    strcat(error, filename);
    perror(error);
  }

  //
  // Quake 3's algorithm to calculate the inverse square root of a number
  //
  float
  fast_inverse_sqrt(float number) {
    long i;
    float x2, y;
    const float threehalfs = 1.5F;

    x2 = number * 0.5F;
    y = number;
    i = * (long *) &y;
    i = 0x5f3759df - (i >> 1);
    y = * (float *) &i;
    y = y * (threehalfs - ( x2 * y * y ));  // can be repeated N times for more precision
    y = y * (threehalfs - ( x2 * y * y ));  // two repetitions already reach float's maximum precision

    return y;
  }

#endif
