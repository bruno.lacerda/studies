#ifndef WINHPW_H_
#define WINHPW_H_

#include <stdio.h>
#include <conio.h>

#define BUF_SIZE 1024

  void
  getpass(char* _str, const size_t bufsize) {
    int c, i = 0;

    do { // hast
      
      c = getch();
      if (c != '\r') _str[i++] = c;

    } while (c != '\r' && i < bufsize-1);
    
    _str[i] = '\0';
  }

  void
  _getpass(char* _str, const int _ch, const size_t bufsize) {
    int c, i = 0;

    do { // hast
      
      c = getch();
      
      if (c != '\r') {
        _str[i++] = c;
        putchar(_ch);
      }

    } while (c != '\r' && i < bufsize-1);
    
    _str[i] = '\0';
    putchar('\n');
  }

  void
  getpass_example() {
    char str[BUF_SIZE];
    
    getpass(str, BUF_SIZE);
    puts(str);
    
    _getpass(str, '*', BUF_SIZE);
    puts(str);
  }

#endif
