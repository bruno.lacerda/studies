#include "tools.h"
#include <unistd.h>
#include <pthread.h>

#define THREAD_COUNT 12

pthread_mutex_t mutex;
static time_t seed;

void *roll_dice() {
  int *retval = (int *)calloc(1, sizeof(int));

  pthread_mutex_lock(&mutex);
  srand(seed++);
  pthread_mutex_unlock(&mutex);

  *retval = (rand() % 6) + 1;

  return (void *)retval;
}

int main(int argc, char const *argv[]) {
  pthread_t thr[THREAD_COUNT];
  int *result = NULL;

  seed = time(NULL);

  pthread_mutex_init(&mutex, NULL);

  for (size_t i = 0; i < THREAD_COUNT; i++) {
    if (pthread_create(thr + i, NULL, &roll_dice, NULL)) { // thr + i === &thr[i]
      return 1;
    }
  }

  for (size_t i = 0; i < THREAD_COUNT; i++) {
    if (pthread_join(thr[i], (void **)&result)) {
      return 2;
    }
    printf(" %d", *result);
  }

  pthread_mutex_destroy(&mutex);
  free(result);

  return 0;
}
