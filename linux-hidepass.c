#include <unistd.h>
#include "lnxhpw.h"

int
main(int argc, char const *argv[]) {
  // Deprecated
  // char* str;
  // puts("getpass");
  // str = getpass(str);

  char pw[MAXPW] = {0};
  char *p = pw;
  ssize_t nchr = 0;

  printf( "\n Enter password: ");
  nchr = getpasswd (&p, MAXPW, '*', stdin);
  printf("\n you entered   : %s  (%zu chars)\n", p, nchr);

  printf( "\n Enter password: ");
  nchr = getpasswd (&p, MAXPW, 0, stdin);
  printf("\n you entered   : %s  (%zu chars)\n\n", p, nchr);

  return 0;
}
