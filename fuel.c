#include "tools.h"
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#define THREAD_COUNT 7

pthread_mutex_t mutexFuel;
pthread_cond_t condFuel;

int fuel = 0;

void*
fuel_filling(void *arg) {
  for (int i = 0; i < 5; i++) {
    pthread_mutex_lock(&mutexFuel);

    fuel += 60;
    printf("Filled fuel... %d\n", fuel);

    pthread_mutex_unlock(&mutexFuel);
    // pthread_cond_signal(&condFuel); // -> signal to only one thread
    pthread_cond_broadcast(&condFuel); // -> broadcasts to all threads
    sleep(1);
  }
}

void*
car(void *arg) {
  pthread_mutex_lock(&mutexFuel);

  while (fuel < 40) {
    printf("No fuel. Waiting...\n");
    pthread_cond_wait(&condFuel, &mutexFuel);
    // Equivalent to:
    // pthread_mutex_unlock(&mutexFuel);
    // wait for signal on condFuel
    // pthread_mutex_lock(&mutexFuel);
  }

  fuel -= 40;
  printf("\nGot fuel. Now left: %d\n", fuel);

  pthread_mutex_unlock(&mutexFuel);
}

int
main(int argc, char *argv[]) {
  pthread_t th[THREAD_COUNT];

  pthread_mutex_init(&mutexFuel, NULL);
  pthread_cond_init(&condFuel, NULL);

  for (int i = 0; i < THREAD_COUNT; i++) {
    if (i == THREAD_COUNT - 1) {
      if (pthread_create(&th[i], NULL, &fuel_filling, NULL)) {
        perror("Failed to create thread");
      }
    } else {
      if (pthread_create(&th[i], NULL, &car, NULL)) {
        perror("Failed to create thread");
      }
    }
  }

  for (int i = 0; i < THREAD_COUNT; i++) {
    if (pthread_join(th[i], NULL)) {
      perror("Failed to join thread");
    }
  }

  pthread_mutex_destroy(&mutexFuel);
  pthread_cond_destroy(&condFuel);

  return 0;
}
