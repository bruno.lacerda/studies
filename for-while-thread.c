#include "tools.h"
#include <pthread.h>

#define THREAD_COUNT 24
#define ONE_BILLION 10000000000
typedef unsigned long long int big_n;

#define n ONE_BILLION

void*
while_sum_short(void *arg) {
  big_n sum = 0;
  size_t i = 0;
  clock_t t;

  t = clock();
  while (i < n) { sum += i++; }
  t = clock() - t;
  printf("\n[while - short] Time taken: %6.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);
}

void*
while_sum_long(void *arg) {
  big_n sum = 0;
  size_t i = 0;
  clock_t t;

  t = clock();
  while (i < n) {
    sum += i;
    i++;
  }
  t = clock() - t;
  printf("\n[while - long ] Time taken: %6.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);
}

void*
for_sum_short(void *arg) {
  big_n sum = 0;
  clock_t t;

  t = clock();
  for (size_t i = 0; i < n;) {
    sum += i++;
  }
  t = clock() - t;
  printf("\n[  for - short] Time taken: %6.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);
}

void*
for_sum_long(void *arg) {
  big_n sum = 0;
  clock_t t;

  t = clock();
  for (size_t i = 0; i < n; i++) {
    sum += i;
  }
  t = clock() - t;
  printf("\n[  for - long ] Time taken: %6.3lf seconds\n", ((double)t) / CLOCKS_PER_SEC);
}

/*
 * OS: Windows 11
 *
 * CPU: AMD Ryzen 5 3600 - 3.95 GHz (Underclock)
 *
 * Average Results [Runs = 8]
 *
 * while - short ->  6.520
 *   for - short ->  6.423
 * while - long  -> 10.199
 *   for - long  -> 10.265
 *
 * ==============================================
 *
 * CPU: AMD Ryzen 9 5900X - (3.7 - 5.0 GHz Auto OC)
 *
 * Average Results [Runs = 2]
 *
 * while - short -> 4.390
 *   for - short -> 4.560
 * while - long  -> 6.027
 *   for - long  -> 5.995
 */

int
main() {
  pthread_t thr[THREAD_COUNT];

  printf("N = %llu\n", n);

  if (pthread_create(&thr[0], NULL, &while_sum_short, NULL)) return 1;
  if (pthread_create(&thr[1], NULL, &while_sum_long,  NULL)) return 2;
  if (pthread_create(&thr[2], NULL, &for_sum_short,   NULL)) return 3;
  if (pthread_create(&thr[3], NULL, &for_sum_long,    NULL)) return 4;

  for (size_t i = 0; i < 4; i++) {
    if (pthread_join(thr[i], NULL)) return -1;
  }

  return 0;
}
