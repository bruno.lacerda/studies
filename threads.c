#include "tools.h"
#include <unistd.h>
#include <pthread.h>

#define THREAD_COUNT 4

int mails = 0;
pthread_mutex_t mutex;

void routine() {
    // puts("Thread initialized");
    
    for (size_t i = 0; i < 10000000; i++) {
        pthread_mutex_lock(&mutex);
        // checks if the mutex is already locked
        // if it is, then the thread is paused until a change happens
        
        mails++;
        // read mails
        // increment
        // write mails

        pthread_mutex_unlock(&mutex);
    }
    
    // puts("Thread finished");
}

int main(int argc, char const *argv[]) {
    pthread_t thr[THREAD_COUNT];

    pthread_mutex_init(&mutex, NULL);

    for (size_t i = 0; i < THREAD_COUNT; i++) {
        if (pthread_create(thr + i, NULL, &routine, NULL)) { // thr + i === &thr[i]
            return 1;
        }

        printf("Thread %d -> INITIALIZED\n", i);
    }

    for (size_t i = 0; i < THREAD_COUNT; i++) {
        if (pthread_join(thr[i], NULL)) {
            return 2;
        }
        
        printf("Thread %d -> FINISHED\n", i);
    }

    printf("Number of mails: %d\n", mails);
    pthread_mutex_destroy(&mutex);

    return 0;
}
