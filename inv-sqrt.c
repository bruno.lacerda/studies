#include "tools.h"

int
main(int argc, char const *argv[]) {
  float n, result;

  n = 2393.0F;
  result = fast_inverse_sqrt(n);
  printf("\nnumero: %.0lf\n  raiz  = %9.6lf\n 1/raiz = %9.6lf\n\n", n, result*n, result);

  return 0;
}